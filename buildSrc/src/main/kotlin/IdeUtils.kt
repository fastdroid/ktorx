/*
 * Copyright 2014-2021 JetBrains s.r.o and contributors. Use of this source code is governed by the Apache 2.0 license.
 */
import org.jetbrains.kotlin.gradle.dsl.*
import org.jetbrains.kotlin.gradle.plugin.mpp.*
import org.jetbrains.kotlin.konan.target.*

fun KotlinMultiplatformExtension.createIdeaTarget(name: String): KotlinNativeTarget = when (HostManager.host) {
    is KonanTarget.LINUX_X64 -> linuxX64(name)
    is KonanTarget.LINUX_ARM32_HFP -> linuxArm32Hfp(name)
    is KonanTarget.LINUX_ARM64 -> linuxArm64(name)
    is KonanTarget.LINUX_MIPS32 -> linuxMips32(name)
    is KonanTarget.LINUX_MIPSEL32 -> linuxMipsel32(name)
    is KonanTarget.ANDROID_ARM32 -> androidNativeArm32(name)
    is KonanTarget.ANDROID_ARM64 -> androidNativeArm64(name)
    is KonanTarget.ANDROID_X86 -> androidNativeX86(name)
    is KonanTarget.ANDROID_X64 -> androidNativeX64(name)
    is KonanTarget.MACOS_X64 -> macosX64(name)
    is KonanTarget.MACOS_ARM64 -> macosArm64(name)
    is KonanTarget.IOS_ARM32 -> iosArm32(name)
    is KonanTarget.IOS_ARM64 -> iosArm64(name)
    is KonanTarget.IOS_X64 -> iosX64(name)
    is KonanTarget.IOS_SIMULATOR_ARM64 -> iosSimulatorArm64(name)
    is KonanTarget.TVOS_ARM64 -> tvosArm64(name)
    is KonanTarget.TVOS_X64 -> tvosX64(name)
    is KonanTarget.TVOS_SIMULATOR_ARM64 -> tvosSimulatorArm64(name)
    is KonanTarget.WATCHOS_ARM32 -> watchosArm32(name)
    is KonanTarget.WATCHOS_ARM64 -> watchosArm64(name)
    is KonanTarget.WATCHOS_X86 -> watchosX86(name)
    is KonanTarget.WATCHOS_X64 -> watchosX64(name)
    is KonanTarget.WATCHOS_SIMULATOR_ARM64 -> watchosSimulatorArm64(name)
    is KonanTarget.MINGW_X64 -> mingwX64(name)
    is KonanTarget.MINGW_X86 -> mingwX86(name)
    else -> error("Unsupported target ${HostManager.host}")
}
